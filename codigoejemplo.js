// Reference HANA driver in the Node app and update the connection details
//fuente del codigo https://developers.sap.com/tutorials/hxe-node-express.html
//install node and npm
//npm install @sap/hdbext modificado por ronny
let hdbext = require('@sap/hdbext')

// Modify the host IP and password based on your system information

let hanaConfig = {

}

// select from a sample table
let sql = "query"

// Execute the query and output the results
// Note: this code doesn't handle any errors (e.g. connection failures etc.,)
hdbext.createConnection(hanaConfig, function (error, client) {
    if (error) {
        return console.error(error);
    }

    client.exec(sql, function (error, rows) {
        console.log('Results:', rows)
    })
})
